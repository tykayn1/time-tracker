console.log('hello console for main.js');
var stuff = ['initialstuff'];
isProduction = false;
angular
  .module('timeTracker', [])
  .controller('MainCtrl', ['$scope', '$http', '$interval', function ($scope, $http, $interval) {

      $scope.urlPrefix = isProduction ? 'https://time.cipherbliss.com/app.php' : 'http://time.lan/app_dev.php/';

      $scope.activities = [
          // {
          //     name: 'lecture',
          //     rank: '10',
          // }, {
          //     name: 'boulot',
          //     rank: '9',
          // }, {
          //     name: 'pause',
          //     rank: '8',
          // }, {
          //     name: 'amis',
          //     rank: '7',
          // }, {
          //     name: 'sport',
          //     rank: '6',
          // }, {
          //     name: 'cuisine',
          //     rank: '5',
          // }, {
          //     name: 'glandage',
          //     rank: '0',
          // },
      ];   // loaded activities
      $scope.paintingActivity = $scope.activities[0];
      $scope.recordingActivity = {};
      $scope.now = new Date();
      $scope.recordingLength = 0;
      $scope.timeout = 0;
      $scope.fetching = false;
      $scope.startDate = null;
      $scope.intervalCounter = null;
      $scope.lastStopDate = null;
      $scope.goalsOfTheWeek = [
          {name: 'faire un truc', achieved: false},
          {name: 'faire un autre truc', achieved: true},
      ];
      $scope.tableTime = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

      $scope.currentDate = new Date();
      /**
       * wait a little before starting a real activity record
       */
      $scope.waitALittle = function () {
          $interval.cancel($scope.wait);
          $scope.timeout = 2;
          $scope.wait = $interval(function () {
              $scope.timeout--;
              if ($scope.timeout < 1) {
                  $interval.cancel($scope.wait);
                  // do the call to record in the backend the new activity
                  $scope.createRecord($scope.recordingActivity);
              }
          }, 1000)
      };

      $scope.createRecord = function (activity) {
          if (!$scope.fetching) {

              var lesParams = {
                  'name' : activity.name,
                  'start': $scope.startDate
              };
              $http({
                  method : 'POST',
                  url    : $scope.urlPrefix + 'create-record',
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data   : lesParams  // pass in data as strings
              }).then(function (rep) {
                  // if successful, bind success message to message
                  $scope.successMessage = rep.data.message;
                  $scope.countRecords = rep.data.new_records_count;
                  $scope.fetching = false;
              }, function (rep) {
                  console.log('nope! ', rep.data);
                  $scope.fetching = false;
              })
              ;
          }
      };

      $scope.recordStart = function (activity) {

          $scope.recordingActivity = activity;
          $scope.startDate = new Date();
// if something previous is recording, stop it*
//           if ($scope.intervalCounter) {
//
//               $interval.cancel($scope.intervalCounter);
//           }
//           $scope.intervalCounter = $interval(function () {
//               $scope.recordingLength = $scope.startDate ? Math.abs($scope.now.getTime() - $scope.startDate.getTime()) / 1000 : 0;
//               console.log('$scope.recordingLength', $scope.recordingLength)
//           }, 1000);
//           $scope.waitALittle();
          $scope.createRecord(activity);
          // record new thing

      };
      $scope.recordStop = function (activity) {
          $scope.recordingActivity = null;
          if (!$scope.fetching) {
              // TODO fetch backend
              $scope.lastStopDate = new Date();
          }

      };

      /**
       * use a certain activity to paint
       * @param activity
       */
      $scope.paintWithActivity = function (activity) {
          // TODO fetch backend
          $scope.paintingActivity = activity;
      };
      // http related calls
      $scope.fetch = function () {
          console.log('fetch products...');
          $http.get($scope.urlPrefix + 'get-my-data').then((rep) => {
                //done
                $scope.initLoadDone = true;
                $scope.activities = rep.data.activities;
                $scope.goalsOfTheWeek = rep.data.goals;
                $scope.fetching = false;
            }, (err) => {
                console.log(err);
                $scope.initLoadDone = true;
                $scope.fetching = false;
            }
          )
      };
      /**
       * create a new activity on the fly
       */
      $scope.addActivity = function () {
          $scope.activities.push({name: '', rank: $scope.activities.length})
      };
      $scope.sendForm = function () {
          // var lesParams = {};
          // $http({
          //     method : 'POST',
          //     url    : $scope.urlPrefix+'add-selling',
          //     headers: {
          //         'Content-Type': 'application/json'
          //     },
          //     data   : lesParams  // pass in data as strings
          // }).then(function (rep) {
          //     // if successful, bind success message to message
          //     $scope.successMessage = rep.data.message;
          //$scope.fetching = false;
          // }, function (rep) {
          //     console.log('nope! ', rep.data);
          // $scope.fetching = false;
          // })
          // ;
      };

      /**
       * set or unset done for a goal
       */
      $scope.toggleGoal = function (id) {
          if (!$scope.fetching) {
              $scope.fetching = true;
              var lesParams = {};
              $http({
                  method : 'POST',
                  url    : $scope.urlPrefix + 'toggleGoal',
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data   : lesParams  // pass in data as strings
              }).then(function (rep) {
                  // if successful, bind success message to message
                  $scope.successMessage = rep.data.message;
                  $scope.fetching = false;
              }, function (rep) {
                  console.log('nope! ', rep.data);
                  $scope.fetching = false;
              })
              ;
          }
      };
      $scope.init = (function () {
          $scope.fetch();
      })();
  }]);

