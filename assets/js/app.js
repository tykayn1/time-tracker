require('../../node_modules/canvasjs/dist/canvasjs.min');
require('../../node_modules/jquery/dist/jquery.min');
require('../../node_modules/introjs/lib/index');

require('../../node_modules/angular/angular.min.js');
require('./parts/main.js');
require('./parts/introtour.js');
require('../css/app.scss');
