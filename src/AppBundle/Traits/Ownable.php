<?php

namespace AppBundle\Traits;

trait Ownable {
	private $owner;

	/**
	 * Set owner.
	 *
	 * @param \AppBundle\Entity\User|null $owner
	 *
	 * @return Record
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Get owner.
	 *
	 * @return \AppBundle\Entity\User|null
	 */
	public function getOwner() {
		return $this->owner;
	}
}
