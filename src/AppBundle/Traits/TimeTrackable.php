<?php

namespace AppBundle\Traits;

trait TimeTrackable {

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="start", type="datetime")
	 */
	private $start;

	/**
	 * @var \DateTime|null
	 *
	 * @ORM\Column(name="end", type="datetime", nullable=true)
	 */
	private $end;

	/**
	 * Set start.
	 *
	 * @param \DateTime $start
	 *
	 * @return Record
	 */
	public function setStart( $start ) {
		$this->start = $start;

		return $this;
	}

	/**
	 * Get start.
	 *
	 * @return \DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end.
	 *
	 * @param \DateTime|null $end
	 *
	 * @return Record
	 */
	public function setEnd( $end = null ) {
		$this->end = $end;

		return $this;
	}

	/**
	 * Get end.
	 *
	 * @return \DateTime|null
	 */
	public function getEnd() {
		return $this->end;
	}

}
