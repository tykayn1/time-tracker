<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 * owner of records, goals and activites
 *
 * @ORM\Table(name="custom_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * domains of activity to record time on
	 * @ORM\OneToMany(targetEntity="Activity", mappedBy="owner")
	 */
	private $activities;

	/**
	 * durations of activities recorded
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Record", mappedBy="owner")
	 */
	private $records;
	/**
	 * durations of activities recorded
	 * @ORM\OneToMany(targetEntity="Goal", mappedBy="owner")
	 */
	private $goals;


	/**
	 * Add activite.
	 *
	 * @param \AppBundle\Entity\Activity $activite
	 *
	 * @return User
	 */
	public function addActivity( \AppBundle\Entity\Activity $activite ) {
		$this->activities[] = $activite;

		return $this;
	}

	/**
	 * Remove activite.
	 *
	 * @param \AppBundle\Entity\Activity $activite
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeActivity( \AppBundle\Entity\Activity $activite ) {
		return $this->activities->removeElement( $activite );
	}

	/**
	 * Get activites.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getActivities() {
		return $this->activities;
	}

	/**
	 * Add record.
	 *
	 * @param \AppBundle\Entity\Record $record
	 *
	 * @return User
	 */
	public function addRecord( \AppBundle\Entity\Record $record ) {
		$this->records[] = $record;

		return $this;
	}

	/**
	 * Remove record.
	 *
	 * @param \AppBundle\Entity\Record $record
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeRecord( \AppBundle\Entity\Record $record ) {
		return $this->records->removeElement( $record );
	}

	/**
	 * Get records.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getRecords() {
		return $this->records;
	}

	/**
	 * Add goal.
	 *
	 * @param \AppBundle\Entity\Goal $goal
	 *
	 * @return User
	 */
	public function addGoal( \AppBundle\Entity\Goal $goal ) {
		$this->goals[] = $goal;

		return $this;
	}

	/**
	 * Remove goal.
	 *
	 * @param \AppBundle\Entity\Goal $goal
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeGoal( \AppBundle\Entity\Goal $goal ) {
		return $this->goals->removeElement( $goal );
	}

	/**
	 * Get goals.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getGoals() {
		return $this->goals;
	}
}
