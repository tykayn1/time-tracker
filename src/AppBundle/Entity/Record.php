<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimeTrackable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Record
 * time span for an activity
 * @ORM\Table(name="record")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecordRepository")
 */
class Record {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;


	/**
	 * durations of activities recorded
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="records")
	 */
	private $owner;

	/**
	 * durations of activities recorded
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Activity", inversedBy="records")
	 */
	private $activity;

	use TimeTrackable;

	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}


	/**
	 * Set activities.
	 *
	 * @param \AppBundle\Entity\Activity|null $activities
	 *
	 * @return Record
	 */
	public function setActivity( \AppBundle\Entity\Activity $activities = null ) {
		$this->activity = $activities;

		return $this;
	}

	/**
	 * Get activities.
	 *
	 * @return \AppBundle\Entity\Activity|null
	 */
	public function getActivity() {
		return $this->activity;
	}

	/**
	 * Set owner.
	 *
	 * @param \AppBundle\Entity\User|null $owner
	 *
	 * @return Record
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Get owner.
	 *
	 * @return \AppBundle\Entity\User|null
	 */
	public function getOwner() {
		return $this->owner;
	}
}
