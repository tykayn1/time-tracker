<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimeTrackable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Goal
 * general task you want to be achieved in a week
 *
 * @ORM\Table(name="goal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GoalRepository")
 */
class Goal {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 */
	private $name;

	/**
	 * @var int|null
	 *
	 * @ORM\Column(name="reportedCount", type="integer", nullable=true)
	 */
	private $reportedCount;

	/**
	 * @var
	 * @ORM\Column( type="boolean", nullable=true)
	 */
	private $achieved;

	/**
	 * @var
	 * @ORM\Column( type="datetime", nullable=true)
	 */
	private $achievedTime;

	use TimeTrackable;
	/**
	 * durations of activities recorded
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="goals")
	 */
	private $owner;


	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Goal
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set reportedCount.
	 *
	 * @param int|null $reportedCount
	 *
	 * @return Goal
	 */
	public function setReportedCount( $reportedCount = null ) {
		$this->reportedCount = $reportedCount;

		return $this;
	}

	/**
	 * Get reportedCount.
	 *
	 * @return int|null
	 */
	public function getReportedCount() {
		return $this->reportedCount;
	}

	/**
	 * Set achieved.
	 *
	 * @param bool|null $achieved
	 *
	 * @return Goal
	 */
	public function setAchieved( $achieved = null ) {
		$this->achieved = $achieved;

		return $this;
	}

	/**
	 * Get achieved.
	 *
	 * @return bool|null
	 */
	public function getAchieved() {
		return $this->achieved;
	}

	/**
	 * Set achievedTime.
	 *
	 * @param \DateTime|null $achievedTime
	 *
	 * @return Goal
	 */
	public function setAchievedTime( $achievedTime = null ) {
		$this->achievedTime = $achievedTime;

		return $this;
	}

	/**
	 * Get achievedTime.
	 *
	 * @return \DateTime|null
	 */
	public function getAchievedTime() {
		return $this->achievedTime;
	}

    /**
     * Set owner.
     *
     * @param \AppBundle\Entity\User|null $owner
     *
     * @return Goal
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
