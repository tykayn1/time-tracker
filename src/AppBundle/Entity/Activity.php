<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 * kind of action to track in time
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActivityRepository")
 */
class Activity {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, unique=true)
	 */
	private $name;
	/**
	 * rank of the quality of the activity. low rank is an activity to avoid, high rank is a fulfilling activity
	 *
	 * @ORM\Column(name="rank", type="integer", unique=true)
	 */
	private $rank;
	/**
	 * we can enable or disable an activity when we dont want to keep on tracking on it
	 *
	 * @ORM\Column(name="enabled", type="boolean", unique=true)
	 */
	private $enabled;

	/**
	 * durations of activities recorded
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="activities")
	 */
	private $owner;
	/**
	 * durations of activities recorded
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Record", mappedBy="activity")
	 */
	private $records;


	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Activity
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set rank.
	 *
	 * @param int $rank
	 *
	 * @return Activity
	 */
	public function setRank( $rank ) {
		$this->rank = $rank;

		return $this;
	}

	/**
	 * Get rank.
	 *
	 * @return int
	 */
	public function getRank() {
		return $this->rank;
	}

	/**
	 * Set owner.
	 *
	 * @param \AppBundle\Entity\User|null $owner
	 *
	 * @return Activity
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Get owner.
	 *
	 * @return \AppBundle\Entity\User|null
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set enabled.
	 *
	 * @param bool $enabled
	 *
	 * @return Activity
	 */
	public function setEnabled( $enabled ) {
		$this->enabled = $enabled;

		return $this;
	}

	/**
	 * Get enabled.
	 *
	 * @return bool
	 */
	public function getEnabled() {
		return $this->enabled;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->records = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add record.
	 *
	 * @param \AppBundle\Entity\Record $record
	 *
	 * @return Activity
	 */
	public function addRecord( \AppBundle\Entity\Record $record ) {
		$this->records[] = $record;

		return $this;
	}

	/**
	 * Remove record.
	 *
	 * @param \AppBundle\Entity\Record $record
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeRecord( \AppBundle\Entity\Record $record ) {
		return $this->records->removeElement( $record );
	}

	/**
	 * Get records.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getRecords() {
		return $this->records;
	}
}
