<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Activity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Activity controller.
 *
 * @Route("activity")
 */
class ActivityController extends Controller {
	/**
	 * Lists all activity entities.
	 *
	 * @Route("/", name="activity_index")
	 * @Method("GET")
	 */
	public function indexAction() {
		$em = $this->getDoctrine()->getManager();

		$activities = $em->getRepository( 'AppBundle:Activity' )->findAll();

		return $this->render( 'activity/index.html.twig',
			[
				'activities' => $activities,
			] );
	}

	/**
	 * Creates a new activity entity.
	 *
	 * @Route("/new", name="activity_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction( Request $request ) {
		$activity = new Activity();
		$activity
			->setEnabled( true )
			->setOwner( $this->getUser() );

		$form = $this->createForm( 'AppBundle\Form\ActivityType', $activity );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->persist( $activity );
			$em->flush();

			return $this->redirectToRoute( 'activity_show', [ 'id' => $activity->getId() ] );
		}

		return $this->render( 'activity/new.html.twig',
			[
				'activity' => $activity,
				'form'     => $form->createView(),
			] );
	}

	/**
	 * Finds and displays a activity entity.
	 *
	 * @Route("/{id}", name="activity_show")
	 * @Method("GET")
	 */
	public function showAction( Activity $activity ) {
		$deleteForm = $this->createDeleteForm( $activity );

		return $this->render( 'activity/show.html.twig',
			[
				'activity'    => $activity,
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Displays a form to edit an existing activity entity.
	 *
	 * @Route("/{id}/edit", name="activity_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction( Request $request, Activity $activity ) {
		if ( $activity->getOwner()->getID() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$deleteForm = $this->createDeleteForm( $activity );
		$editForm   = $this->createForm( 'AppBundle\Form\ActivityType', $activity );
		$editForm->handleRequest( $request );

		if ( $editForm->isSubmitted() && $editForm->isValid() ) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'activity_edit', [ 'id' => $activity->getId() ] );
		}

		return $this->render( 'activity/edit.html.twig',
			[
				'activity'    => $activity,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Deletes a activity entity.
	 *
	 * @Route("/{id}", name="activity_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction( Request $request, Activity $activity ) {
		if ( $activity->getOwner()->getID() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$form = $this->createDeleteForm( $activity );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->remove( $activity );
			$em->flush();
		}

		return $this->redirectToRoute( 'activity_index' );
	}

	/**
	 * Creates a form to delete a activity entity.
	 *
	 * @param Activity $activity The activity entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( Activity $activity ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'activity_delete', [ 'id' => $activity->getId() ] ) )
		            ->setMethod( 'DELETE' )
		            ->getForm();
	}
}
