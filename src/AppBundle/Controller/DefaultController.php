<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Festival;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\ProductSold;
use AppBundle\Entity\Record;
use AppBundle\Entity\SellRecord;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class DefaultController extends Controller {

	private $tokenManager;

	public function __construct( CsrfTokenManagerInterface $tokenManager = null ) {
		$this->tokenManager = $tokenManager;
	}

	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction( Request $request ) {
		$m        = $this->getDoctrine()->getManager();
		$userRepo = $m->getRepository( 'AppBundle:User' );
		$allUsers = $userRepo->findAll();

		// replace this example code with whatever you need
		return $this->render( 'default/main-screen.html.twig',
			[
				'usersCount' => count( $allUsers ),
			] );
	}

	/**
	 * @Route("/dashboard", name="dashboard")
	 */
	public function dashboardAction( Request $request ) {
		$m = $this->getDoctrine()->getManager();

		// replace this example code with whatever you need
		return $this->render( 'default/pages/dashboard.html.twig',
			[] );
	}

	/**
	 * get my activities data
	 * @Route("/get-my-data", name="get_my_data")
	 */
	public function getMyProductsAction( Request $request ) {
		$m = $this->getDoctrine()->getManager();

		$activities        = $m->getRepository( 'AppBundle:Activity' );
		$myActivities      = $activities->findBy( [ 'owner' => $this->getUser()->getId(), 'enabled' => true ],
			[ 'rank' => 'desc' ] );
		$myActivitiesFound = [];
		foreach ( $myActivities as $my_activity ) {
			$myActivitiesFound[] = [
				'name' => $my_activity->getName(),
				'rank' => $my_activity->getRank(),
			];
		}

		$goals        = $m->getRepository( 'AppBundle:Goal' );
		$myGoals      = $goals->findBy( [ 'owner' => $this->getUser()->getId() ], [ 'start' => 'desc' ] );
		$myGoalsFound = [];
		foreach ( $myGoals as $my_goal ) {
			$myGoalsFound[] = [
				'name'     => $my_goal->getName(),
				'achieved' => $my_goal->getAchieved(),
			];
		}

		$json = [
			'activities'        => $myActivitiesFound,
			'goals'             => $myGoalsFound,
			'new_records_count' => count( $this->getUser()->getRecords() ),
			'recording_now'     => [
				'name'       => 'exemple',
				'started_at' => new \DateTime(),
			],
		];

		return new JsonResponse( $json );
	}

	/**
	 * @Route("/create-record", name="createRecord")
	 */
	public function createRecordAction( Request $request ) {
		$m = $this->getDoctrine()->getManager();
		// find activity
		$repo          = $m->getRepository( 'AppBundle:Activity' );
		$params        = json_decode( $request->getContent() );
		$name          = $params->name;
		$foundActivity = $repo->findOneByName( $name );
		if ( $foundActivity ) {
			$currentUser = $this->getUser();
			$record      = new Record();
			$record
				->setStart( new \DateTime() )
				->setOwner( $currentUser )
				->setActivity( $foundActivity );
			$foundActivity->addRecord( $record );
			$currentUser->addRecord( $record );

			$m->persist( $record );
			$m->persist( $currentUser );
			$m->persist( $foundActivity );

			$m->flush();

			$json   = [ 'message' => 'ok created ', 'new_records_count' => count( $currentUser->getRecords() ) ];
			$status = 200;
		} else {
			$json   = [ 'message' => 'activity "' . $name . '" not found' ];
			$status = 404;
		}


		return new JsonResponse( $json, $status );
	}

	/**
	 * @Route("/stats", name="stats")
	 */
	public function statsAction( Request $request ) {
		$m         = $this->getDoctrine()->getManager();
		$repoGoals = $m->getRepository( 'AppBundle:Goal' );

		// TODO find successful goals, sort them by most recently achieved
		// TODO find not successful goals, sort them by most recently created
		// TODO list activities record during last month, and make stats for them

		$views = [];

		return $this->render( 'default/pages/stats.html.twig',
			$views );
	}


}
