<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Record;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Record controller.
 *
 * @Route("record")
 */
class RecordController extends Controller {
	/**
	 * Lists all record entities.
	 *
	 * @Route("/", name="record_index")
	 * @Method("GET")
	 */
	public function indexAction() {
		$em = $this->getDoctrine()->getManager();

		$records = $em->getRepository( 'AppBundle:Record' )->findAll();

		return $this->render( 'record/index.html.twig',
			[
				'records' => $records,
			] );
	}

	/**
	 * Creates a new record entity.
	 *
	 * @Route("/new", name="record_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction( Request $request ) {
		$record = new Record();
		$form   = $this->createForm( 'AppBundle\Form\RecordType', $record );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->persist( $record );
			$em->flush();

			return $this->redirectToRoute( 'record_show', [ 'id' => $record->getId() ] );
		}

		return $this->render( 'record/new.html.twig',
			[
				'record' => $record,
				'form'   => $form->createView(),
			] );
	}

	/**
	 * Finds and displays a record entity.
	 *
	 * @Route("/{id}", name="record_show")
	 * @Method("GET")
	 */
	public function showAction( Record $record ) {
		$deleteForm = $this->createDeleteForm( $record );

		return $this->render( 'record/show.html.twig',
			[
				'record'      => $record,
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Displays a form to edit an existing record entity.
	 *
	 * @Route("/{id}/edit", name="record_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction( Request $request, Record $record ) {

		if ( $record->getOwner()->getID() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$deleteForm = $this->createDeleteForm( $record );
		$editForm   = $this->createForm( 'AppBundle\Form\RecordType', $record );
		$editForm->handleRequest( $request );

		if ( $editForm->isSubmitted() && $editForm->isValid() ) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'record_edit', [ 'id' => $record->getId() ] );
		}

		return $this->render( 'record/edit.html.twig',
			[
				'record'      => $record,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Deletes a record entity.
	 *
	 * @Route("/{id}", name="record_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction( Request $request, Record $record ) {
		if ( $record->getOwner()->getID() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$form = $this->createDeleteForm( $record );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->remove( $record );
			$em->flush();
		}

		return $this->redirectToRoute( 'record_index' );
	}

	/**
	 * Creates a form to delete a record entity.
	 *
	 * @param Record $record The record entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( Record $record ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'record_delete', [ 'id' => $record->getId() ] ) )
		            ->setMethod( 'DELETE' )
		            ->getForm();
	}
}
