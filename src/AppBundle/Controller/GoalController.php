<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Goal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Goal controller.
 *
 * @Route("goal")
 */
class GoalController extends Controller {
	/**
	 * Lists all goal entities.
	 *
	 * @Route("/", name="goal_index")
	 * @Method("GET")
	 */
	public function indexAction() {
		$em = $this->getDoctrine()->getManager();

		$goals = $em->getRepository( 'AppBundle:Goal' )->findAll();

		return $this->render( 'goal/index.html.twig',
			[
				'goals' => $goals,
			] );
	}

	/**
	 * Creates a new goal entity.
	 *
	 * @Route("/new", name="goal_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction( Request $request ) {
		$goal = new Goal();
		$goal->setOwner( $this->getUser() );

		$form = $this->createForm( 'AppBundle\Form\GoalType', $goal );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$goal->setStart( new \DateTime() );
			if ( $goal->getAchieved() ) {
				$goal->setAchievedTime( new \DateTime() );
			}
			$em->persist( $goal );
			$em->flush();

			return $this->redirectToRoute( 'goal_show', [ 'id' => $goal->getId() ] );
		}

		return $this->render( 'goal/new.html.twig',
			[
				'goal' => $goal,
				'form' => $form->createView(),
			] );
	}

	/**
	 * Finds and displays a goal entity.
	 *
	 * @Route("/{id}", name="goal_show")
	 * @Method("GET")
	 */
	public function showAction( Goal $goal ) {
		$deleteForm = $this->createDeleteForm( $goal );

		return $this->render( 'goal/show.html.twig',
			[
				'goal'        => $goal,
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Displays a form to edit an existing goal entity.
	 *
	 * @Route("/{id}/edit", name="goal_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction( Request $request, Goal $goal ) {
		if ( $goal->getOwner()->getID() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$deleteForm = $this->createDeleteForm( $goal );
		$editForm   = $this->createForm( 'AppBundle\Form\GoalType', $goal );
		$editForm->handleRequest( $request );

		if ( $editForm->isSubmitted() && $editForm->isValid() ) {
			if ( $goal->getAchieved() ) {
				$goal->setAchievedTime( new \DateTime() );
			}
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'goal_edit', [ 'id' => $goal->getId() ] );
		}

		return $this->render( 'goal/edit.html.twig',
			[
				'goal'        => $goal,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Deletes a goal entity.
	 *
	 * @Route("/{id}", name="goal_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction( Request $request, Goal $goal ) {
		if ( $goal->getOwner()->getID() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$form = $this->createDeleteForm( $goal );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->remove( $goal );
			$em->flush();
		}

		return $this->redirectToRoute( 'goal_index' );
	}

	/**
	 * Creates a form to delete a goal entity.
	 *
	 * @param Goal $goal The goal entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( Goal $goal ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'goal_delete', [ 'id' => $goal->getId() ] ) )
		            ->setMethod( 'DELETE' )
		            ->getForm();
	}
}
