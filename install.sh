#!/bin/bash
echo "checking requirements";
composer --version
yarn --version
acl --varsion

echo "installing symfony project for ubuntu environnement";
composer install && yarn install;
yarn run encore dev
echo "fix file permissions";
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var

echo "update database";
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
